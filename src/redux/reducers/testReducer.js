import { actionTypes } from '../constants/actionTypes';

const initialState = {
	test: true,
};

export default function testReducer(state = initialState, action) {
	switch (action.type) {
		case actionTypes.THIS_IS_TEST:
			return {
				...state,
				disable: action.payload,
			};
		default:
			return state;
	}
}
