import { applyMiddleware, compose, createStore } from 'redux';
import reduxThunk from 'redux-thunk';
import rootReducer from './reducers.js';

const middlewares = [reduxThunk];

export const store = createStore(
	rootReducer,
	compose(
		applyMiddleware(...middlewares),
		window.devToolsExtension ? window.devToolsExtension({ name: 'Test App' }) : f => f
	)
);


