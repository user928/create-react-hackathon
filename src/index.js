import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter, Match, Miss } from 'react-router';

import './css/style.css';
import '../node_modules/antd/dist/antd.css';
import './css/vendor/bootstrap3.css'
import './css/base.css'
import './css/shared.css'

import App from './components/App/App';
import NotFound from './components/NotFound/NotFound';
import DetailsPage from "./components/DetailsPage/DetailsPage";

const Root = () => {
	return (
		<BrowserRouter>
			<div>
				<Match exactly pattern="/" component={App} />
				<Match pattern="/details-page" component={DetailsPage} />
				<Miss component={NotFound} />
			</div>
		</BrowserRouter>
	)
};

render(<Root />, document.querySelector('#main'));
