import React from 'react';

import { connect, Provider } from 'react-redux';
import { bindActionCreators } from 'redux';
import { store } from '../../redux/store';

import './detailsPage.css';

class _DetailsPage extends React.Component {

	render() {
		return (
			<div className="container-fluid">
                <div className="row">
                    <h1 className="col-sm-12">Details Page Demo Txt</h1>
                </div>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {};
};

const mapDispatchToProps = dispatch => {
	return bindActionCreators(
		{},
		dispatch
	);
};

const DetailsPageComponent = connect(mapStateToProps, mapDispatchToProps)(_DetailsPage);

export default class MoviePage extends React.Component {
	render() {
		return (
			<Provider store={store}>
				<DetailsPageComponent {...this.props} />
			</Provider>
		);
	}
}
