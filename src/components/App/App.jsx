import React from 'react';
import { bindActionCreators } from 'redux';
import { connect, Provider } from 'react-redux';
import { store } from '../../redux/store';

import Button from 'antd/lib/button';

class _App extends React.Component {

	render() {
		return (
			<div className="container-fluid">
				<div className="ro">
                    <div className="col-sm-12">
                        <h1>This is some text...</h1>
                        This is some button <Button type="primary">Button</Button>
                    </div>
                </div>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {};
};

const mapDispatchToProps = dispatch => {
	return bindActionCreators(
		{},
		dispatch
	);
};

const AppComponent = connect(mapStateToProps, mapDispatchToProps)(_App);

export default class App extends React.Component {
	render() {
		return (
			<Provider store={store}>
				<AppComponent {...this.props} />
			</Provider>
		);
	}
}
